// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 3 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
#ifndef DUMUX_PHASEFIELD_INDICES_HH
#define DUMUX_PHASEFIELD_INDICES_HH

namespace Dumux {
// \{

template<int offset = 0>
struct PhasefieldIndices
{
    static const int PVOffset = offset;      //!< the first index in primary variable vector
    static const int p1Idx = PVOffset; //!< index of the component phi1 for 3 variable formulation
    static const int p2Idx = PVOffset + 1; //!< index of the component phi2
    static const int p3Idx = PVOffset + 2; //!< index of the component phi3
#if !ODE
    static const int uAIdx = PVOffset + 3; //!< index of the component u_A
    static const int uBIdx = PVOffset + 4; //!< index of the component u_B
    static const int uCIdx = PVOffset + 5; //!< index of the component u_C
#endif
};

// \}
} // end namespace

#endif
