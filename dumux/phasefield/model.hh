// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 3 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
#ifndef DUMUX_PHASEFIELD_MODEL_HH
#define DUMUX_PHASEFIELD_MODEL_HH

#include <dumux/common/properties.hh>
#include <dumux/common/properties/model.hh>
#include <dumux/flux/fluxvariablescaching.hh>

#include "indices.hh"
#include "volumevariables.hh"
#include "iofields.hh"
#include "localresidual.hh"

namespace Dumux {

struct PhasefieldModelTraits
{
    using Indices = PhasefieldIndices<>;

#if ODE
    static constexpr int numEq() { return 3; }
#else
    static constexpr int numEq() { return 6; }
#endif
    static constexpr int numComponents() { return numEq(); }
};

template<class PV, class MT>
struct PhasefieldVolumeVariablesTraits
{
    using PrimaryVariables = PV;
    using ModelTraits = MT;
};

namespace Properties {
namespace TTag {
struct Phasefield { using InheritsFrom = std::tuple<ModelProperties>; };
}

template<class TypeTag>
struct IOFields<TypeTag, TTag::Phasefield> { using type = PhasefieldIOFields; };

template<class TypeTag>
struct LocalResidual<TypeTag, TTag::Phasefield> { using type = PhasefieldLocalResidual<TypeTag>; };

template<class TypeTag>
struct ModelTraits<TypeTag, TTag::Phasefield> { using type = PhasefieldModelTraits; };

template<class TypeTag>
struct VolumeVariables<TypeTag, TTag::Phasefield>
{
private:
    using PV = GetPropType<TypeTag, Properties::PrimaryVariables>;
    using MT = GetPropType<TypeTag, Properties::ModelTraits>;

    using Traits = PhasefieldVolumeVariablesTraits<PV, MT>;
public:
    using type = PhasefieldVolumeVariables<Traits>;
};

template<class TypeTag>
struct FluxVariablesCache<TypeTag, TTag::Phasefield> { using type = FluxVariablesCaching::EmptyCache<GetPropType<TypeTag, Properties::Scalar>>; };

template<class TypeTag>
struct FluxVariablesCacheFiller<TypeTag, TTag::Phasefield> { using type = FluxVariablesCaching::EmptyCacheFiller; };

} // end namespace Properties
} // end namespace Dumux

#endif
