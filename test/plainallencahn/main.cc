// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
#include "config.h"
#include "problem.hh"

#include <ctime>
#include <iostream>
#include <fstream>

#include <dune/common/parallel/mpihelper.hh>
#include <dune/common/timer.hh>
#include <dune/grid/io/file/vtk.hh>
#include <dune/istl/io.hh>
#include <dune/grid/yaspgrid.hh>

#include <dumux/common/properties.hh>
#include <dumux/common/parameters.hh>
#include <dumux/common/dumuxmessage.hh>
#include <dumux/common/defaultusagemessage.hh>

#include <dumux/linear/seqsolverbackend.hh>
#include <dumux/nonlinear/newtonsolver.hh>

#include <dumux/assembly/fvassembler.hh>
#include <dumux/assembly/diffmethod.hh>

#include <dumux/discretization/method.hh>
#include <dumux/io/grid/gridmanager.hh>
#include <dumux/io/vtkoutputmodule.hh>

#include <dumux/discretization/cctpfa.hh>
#include <dumux/phasefield/model.hh>

template <class Assembler, class SolutionVector, class LocalAssembler>
auto assembleBoundaryFluxes(const Assembler& assembler, const SolutionVector& curSol)
{
    typename SolutionVector::block_type flux(0.0);

    for (const auto& element : elements(assembler.gridView()))
    {
        
        LocalAssembler localAssembler(assembler, element, curSol);

        for (const auto& scvf : scvfs(localAssembler.fvGeometry()))
        {
            if (scvf.boundary())
            {
                flux += localAssembler.localResidual().evalFlux(localAssembler.problem(),
                         element,
                         localAssembler.fvGeometry(),
                         localAssembler.curElemVolVars(),
                         localAssembler.elemFluxVarsCache(),
                         scvf)
                    * scvf.area();
            }
        }
    }

    return flux;
}

namespace Dumux {
namespace Properties {

namespace TTag {
struct PlainAllenCahn { using InheritsFrom = std::tuple<Phasefield, CCTpfaModel/*BoxModel*/>; };
}

template<class TypeTag>
struct Grid<TypeTag, TTag::PlainAllenCahn> { using type = Dune::YaspGrid<2>; };

template<class TypeTag>
struct Problem<TypeTag, TTag::PlainAllenCahn> { using type = PlainAllenCahnProblem<TypeTag>; };

template<class TypeTag>
struct EnableGridVolumeVariablesCache<TypeTag, TTag::PlainAllenCahn> { static constexpr bool value = true; };

template<class TypeTag>
struct EnableGridGeometryCache<TypeTag, TTag::PlainAllenCahn> { static constexpr bool value = true; };

} // end namespace Properties
} // end namespace Dumux

void usage(const char *progName, const std::string &errorMsg)
{
    if (errorMsg.size() > 0) {
        std::string errorMessageOut = "\nUsage: ";
                    errorMessageOut += progName;
                    errorMessageOut += " [options]\n";
                    errorMessageOut += errorMsg;
                    errorMessageOut += "\n\nThe list of important arguments for this program is:\n"
                            "\t-TimeLoop.TEnd             end time of the simulation\n"
                            "\t-TimeLoop.DtInitial        initial time step size\n"
                            "\t-TimeLoop.MaxTimeStepSize  maximal time step size\n"
                            "\t-Grid.LowerLeft            lower left (front) corner of the domain\n"
                            "\t-Grid.UpperRight           upper right (back) corner of the domain\n"
                            "\t-Grid.Cells                grid resolution in each coordinate direction\n"
                            "\t-Problem.xi                phasefield parameter\n"
                            "\t-Problem.omega             phasefield diffusivity/surface tension parameter\n"
                            "\t-Problem.delta             regularization parameter\n"
                            "\t-Problem.rhoD              density mineral D\n"
                            "\t-Problem.rhoP              density mineral P\n"
                            "\t-Problem.DiffCoeff         diffusion coefficient\n"
                            "\t-Problem.OutputInterval    interval size for VTK output\n"
                            "\t-Problem.Name              base name for VTK output files\n";
        std::cout << errorMessageOut
                  << "\n";
    }
}

int main(int argc, char** argv)
{
    using namespace Dumux;

    // define the type tag for this problem
    using TypeTag = Properties::TTag::PlainAllenCahn;

    // initialize MPI, finalize is done automatically on exit
    const auto& mpiHelper = Dune::MPIHelper::instance(argc, argv);

    // print dumux start message
    if (mpiHelper.rank() == 0)
        DumuxMessage::print(/*firstCall=*/true);

    // parse command line arguments and input file
    Parameters::init(argc, argv);

    // try to create a grid (from the given grid file or the input file)
    GridManager<GetPropType<TypeTag, Properties::Grid>> gridManager;
    gridManager.init();

    ////////////////////////////////////////////////////////////
    // run instationary non-linear problem on this grid
    ////////////////////////////////////////////////////////////

    // we compute on the leaf grid view
    const auto& leafGridView = gridManager.grid().leafGridView();

    // create the finite volume grid geometry
    using GridGeometry = GetPropType<TypeTag, Properties::GridGeometry>;
    auto gridGeometry = std::make_shared<GridGeometry>(leafGridView);
    gridGeometry->update(leafGridView);

    // the problem (initial and boundary conditions)
    using Problem = GetPropType<TypeTag, Properties::Problem>;
    auto problem = std::make_shared<Problem>(gridGeometry);

    // get some time loop parameters
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
    const auto tEnd = getParam<Scalar>("TimeLoop.TEnd");
    const auto maxDt = getParam<Scalar>("TimeLoop.MaxTimeStepSize");
    auto dt = getParam<Scalar>("TimeLoop.DtInitial");

    // the solution vector
    using SolutionVector = GetPropType<TypeTag, Properties::SolutionVector>;
    auto xPtr = std::make_shared<SolutionVector>();
    SolutionVector x = *xPtr;
    problem->applyInitialSolution(x);
    auto xOldPtr = std::make_shared<SolutionVector>();
    *xOldPtr = x;

    // the grid variables
    using GridVariables = GetPropType<TypeTag, Properties::GridVariables>;
    auto gridVariables = std::make_shared<GridVariables>(problem, gridGeometry);
    gridVariables->init(x);

    // intialize the vtk output module
    using IOFields = GetPropType<TypeTag, Properties::IOFields>;
    VtkOutputModule<GridVariables, SolutionVector> vtkWriter(*gridVariables, x, problem->name());
    IOFields::initOutputModule(vtkWriter); //!< Add model specific output fields
    vtkWriter.write(0.0);

    // instantiate time loop
    auto timeLoop = std::make_shared<CheckPointTimeLoop<Scalar>>(0.0, dt, tEnd);
    timeLoop->setMaxTimeStepSize(maxDt);

    bool enableCheckPoints = hasParam("Problem.OutputInterval");
    if (enableCheckPoints)
        timeLoop->setPeriodicCheckPoint(getParam<double>("Problem.OutputInterval"));

    // the assembler with time loop for instationary problem
    using Assembler = FVAssembler<TypeTag, DiffMethod::numeric>;
    auto assembler = std::make_shared<Assembler>(problem, gridGeometry, gridVariables, timeLoop, *xOldPtr);

    // the linear solver
    using LinearSolver = UMFPackBackend;
    auto linearSolver = std::make_shared<LinearSolver>();

    // the non-linear solver
    using NewtonSolver = Dumux::NewtonSolver<Assembler, LinearSolver>;
    NewtonSolver nonLinearSolver(assembler, linearSolver);

    //using LocalAssembler = CCLocalAssembler<TypeTag, Assembler, DiffMethod::numeric,
    //      /*implic=*/true>;
    //auto boundaryFlux = assembleBoundaryFluxes<Assembler, SolutionVector, LocalAssembler>(*assembler, x);
    // pass xOld and timeloop to problem to acess time derivatives
    problem->initTimeDerivative(xOldPtr, timeLoop);
#if ODE
    problem->initODE();
    std::ofstream fout_ode;
    fout_ode.open(getParam<std::string>("Problem.Name")+"_ode.txt");
    problem->writeODE(fout_ode);
#endif
    std::ofstream fout_scalar;
    fout_scalar.open(getParam<std::string>("Problem.Name")+"_scalars.txt");
    problem->calculateGlobalScalarMeasures(xOldPtr/*, boundaryFlux*/);
    problem->updateGlobalValues();
    problem->writeScalars(fout_scalar);

    // time loop
    timeLoop->start(); do
    {
        // linearize & solve
        nonLinearSolver.solve(x, *timeLoop);

        // make the new solution the old solution
        *xOldPtr = x;
        gridVariables->advanceTimeStep();

        // update global Sclalars for surface/volume measures
        //auto boundaryFlux = assembleBoundaryFluxes<Assembler, SolutionVector, LocalAssembler>(*assembler, x);
        problem->calculateGlobalScalarMeasures(xOldPtr/*, boundaryFlux*/);

        // solve ODE or simply update to new Volumes, requires calculation of global scalars above
#if ODE
        problem->advanceODE(xOldPtr);
#endif
        problem->updateGlobalValues();

        // advance to the time loop to the next step
        timeLoop->advanceTimeStep();

        // scalar values to file for plotting
        problem->writeScalars(fout_scalar);

#if ODE
        // ode values to file for plotting
        problem->writeODE(fout_ode);
#endif

        // report statistics of this time step
        timeLoop->reportTimeStep();

        // set new dt as suggested by the newton solver
        timeLoop->setTimeStepSize(nonLinearSolver.suggestTimeStepSize(timeLoop->timeStepSize()));

        if (timeLoop->isCheckPoint() || timeLoop->finished() || !enableCheckPoints)
        {
            vtkWriter.write(timeLoop->time());
        }

    } while (!timeLoop->finished());

#if ODE
    fout_ode.close();
#endif
    fout_scalar.close();
    timeLoop->finalize(leafGridView.comm());

    ////////////////////////////////////////////////////////////
    // finalize, print dumux message to say goodbye
    ////////////////////////////////////////////////////////////

    // print dumux end message
    if (mpiHelper.rank() == 0)
    {
        Parameters::print();
        DumuxMessage::print(/*firstCall=*/false);
    }

    return 0;
}
