DuMux module adding a multi-phase Allen-Cahn phase-field formulation with
coupled (Navier-)Stokes flow.

===============================

The module includes simulation setups for three scenarios
 - Geometry evolution of a 3-phase system with two solids and one fluid
 - Two-phase flow
 - Periodic cell problems for diffusion and flow

