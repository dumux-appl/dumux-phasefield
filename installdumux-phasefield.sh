# dune-common
# releases/2.6 # 1b45bfb24873f80bfc7b419c04dea79f93aef1d7 # 2019-03-19 10:11:58 +0000 # Jö Fahlke
git clone https://gitlab.dune-project.org/core/dune-common.git
cd dune-common
git checkout releases/2.6
git reset --hard 1b45bfb24873f80bfc7b419c04dea79f93aef1d7
cd ..

# dune-geometry
# releases/2.6 # 1eb09fa58d4894ead8df4dd235bd3d19f2b6ac38 # 2018-12-06 22:56:21 +0000 # Steffen Müthing
git clone https://gitlab.dune-project.org/core/dune-geometry.git
cd dune-geometry
git checkout releases/2.6
git reset --hard 1eb09fa58d4894ead8df4dd235bd3d19f2b6ac38
cd ..

# dune-uggrid
# releases/2.6 # 07f9700459c616186737a9a34277f2edee76f475 # 2018-04-04 16:53:52 +0200 # Ansgar Burchardt
git clone https://gitlab.dune-project.org/staging/dune-uggrid.git
cd dune-uggrid
git checkout releases/2.6
git reset --hard 07f9700459c616186737a9a34277f2edee76f475
cd ..

# dune-grid
# releases/2.6 # f7983c01afbe9b7c379f9944ecbacb189ac8c6e6 # 2020-02-09 10:28:23 +0000 # Christoph Grüninger
git clone https://gitlab.dune-project.org/core/dune-grid.git
cd dune-grid
git checkout releases/2.6
git reset --hard f7983c01afbe9b7c379f9944ecbacb189ac8c6e6
cd ..

# dune-localfunctions
# releases/2.6 # ee794bfdfa3d4f674664b4155b6b2df36649435f # 2018-12-06 23:40:32 +0000 # Steffen Müthing
git clone https://gitlab.dune-project.org/core/dune-localfunctions.git
cd dune-localfunctions
git checkout releases/2.6
git reset --hard ee794bfdfa3d4f674664b4155b6b2df36649435f
cd ..

# dune-istl
# releases/2.6 # 9698e497743654b6a03c219b2bdfc27b62a7e0b3 # 2018-12-14 10:00:30 +0000 # Jö Fahlke
git clone https://gitlab.dune-project.org/core/dune-istl.git
cd dune-istl
git checkout releases/2.6
git reset --hard 9698e497743654b6a03c219b2bdfc27b62a7e0b3
cd ..

# dune-alugrid
# releases/2.6 # c0851a92b3af8d93a75f798de3d34f65cc895341 # 2018-07-25 11:01:08 +0000 # Martin Alkämper
git clone https://gitlab.dune-project.org/extensions/dune-alugrid.git
cd dune-alugrid
git checkout releases/2.6
git reset --hard c0851a92b3af8d93a75f798de3d34f65cc895341
cd ..

# dune-foamgrid
# releases/2.6 # 20f7851cac039627402419db6cb5850b4485c871 # 2019-05-01 18:45:26 +0000 # Timo Koch
git clone https://gitlab.dune-project.org/extensions/dune-foamgrid.git
cd dune-foamgrid
git checkout releases/2.6
git reset --hard 20f7851cac039627402419db6cb5850b4485c871
cd ..

# opm-common
# master # 4da5a812333ae2e1b65c4f4e59e6237e1444d3f0 # 2020-04-19 23:45:51 +0200 # Bård Skaflestad
git clone https://github.com/OPM/opm-common.git
cd opm-common
git checkout master
git reset --hard 4da5a812333ae2e1b65c4f4e59e6237e1444d3f0
cd ..

# opm-grid
# master # e2511eda3d4ff93a49af968a940e26bbf7bc5348 # 2020-04-15 21:02:22 +0200 # Markus Blatt
git clone https://github.com/OPM/opm-grid.git
cd opm-grid
git checkout master
git reset --hard e2511eda3d4ff93a49af968a940e26bbf7bc5348
cd ..

# dumux
# releases/3.2 # f96ad774215778d6f843f1287f56a35e9c70d59e # 2020-04-20 09:27:32 +0200 # Timo Koch
git clone https://git.iws.uni-stuttgart.de/dumux-repositories/dumux.git
cd dumux
git checkout releases/3.2
git reset --hard f96ad774215778d6f843f1287f56a35e9c70d59e
cd ..

