add_input_file_links()

dune_add_test(NAME test_gierermeinhardt
              SOURCES main.cc
              COMMAND python
              CMD_ARGS ${dumux_INCLUDE_DIRS}/bin/testing/runtest.py
                 --script fuzzy
                 --files ${CMAKE_SOURCE_DIR}/test/references/gierermeinhardt-reference.vtu
                         ${CMAKE_CURRENT_BINARY_DIR}/gierermeinhardt-00001.vtu
                 --command "${CMAKE_CURRENT_BINARY_DIR}/test_gierermeinhardt -TimeLoop.TEnd 0.5")

# headers for installation and headercheck
install(FILES
        problem.hh
DESTINATION ${CMAKE_INSTALL_INCLUDEDIR}/test/gierermeinhardt)
