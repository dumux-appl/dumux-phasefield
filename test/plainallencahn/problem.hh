// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
#ifndef DUMUX_PLAINALLENCAHN_PROBLEM_HH
#define DUMUX_PLAINALLENCAHN_PROBLEM_HH

#include <dumux/common/fvproblem.hh>
#include <dumux/common/timeloop.hh>
#include <dune/istl/matrix.hh>
#include <fstream>
#include <iostream>

#ifndef ODE
#define ODE 1
#endif

#ifndef ODE_REACTION
#define ODE_REACTION 0
#endif

namespace Dumux {

template <class TypeTag >
class PlainAllenCahnProblem : public FVProblem<TypeTag>
{
    using ParentType = FVProblem<TypeTag>;
    using GridView = typename GetPropType<TypeTag, Properties::GridGeometry>::GridView;
    using FVGridGeometry = GetPropType<TypeTag, Properties::GridGeometry>;
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
    using Indices = typename GetPropType<TypeTag, Properties::ModelTraits>::Indices;
    using BoundaryTypes = Dumux::BoundaryTypes<GetPropType<TypeTag,
          Properties::ModelTraits>::numEq()>;
    using PrimaryVariables = GetPropType<TypeTag, Properties::PrimaryVariables>;
    using NumEqVector = Dumux::NumEqVector<PrimaryVariables>;
    using ElementVolumeVariables = typename GetPropType<TypeTag, Properties::GridVolumeVariables>::LocalView;
    using FVElementGeometry = typename GetPropType<TypeTag, Properties::GridGeometry>::LocalView;
    using SubControlVolume = typename FVElementGeometry::SubControlVolume;
    using SolutionVector = GetPropType<TypeTag, Properties::SolutionVector>;
    using TimeLoopPtr = std::shared_ptr<CheckPointTimeLoop<Scalar>>;
    using Matrix = Dune::FieldMatrix<Scalar,3,3>;
    using Vector = Dune::FieldVector<Scalar,3>;

    static constexpr int p1Idx = Indices::p1Idx;
    static constexpr int p2Idx = Indices::p2Idx;
    static constexpr int p3Idx = Indices::p3Idx;
    static constexpr int uAIdx = Indices::uAIdx;
    static constexpr int uBIdx = Indices::uBIdx;
    static constexpr int uCIdx = Indices::uCIdx;

    using Element = typename GridView::template Codim<0>::Entity;
    using GlobalPosition = typename Element::Geometry::GlobalCoordinate;

public:
    PlainAllenCahnProblem( std::shared_ptr<const FVGridGeometry> fvGridGeometry)
    : ParentType(fvGridGeometry)
    {
        omega_ = getParam<Scalar>("Problem.omega");
        alpha_ = 1.0;
        if (getParam<Scalar>("Problem.UseAlpha"))
        {
            alpha_ = 1.0/omega_;
            omega_ = 1.0;
        }
        xi_ = getParam<Scalar>("Problem.xi");
        rhoD_ = getParam<Scalar>("Problem.rhoD");
        rhoP_ = getParam<Scalar>("Problem.rhoP");
        std::tuple<Scalar,Scalar> numCells = getParam<std::tuple<Scalar,Scalar>>("Grid.Cells");
        hx_ = 1.0/std::get<0>(numCells);
        hy_ = 1.0/std::get<1>(numCells);
    }

    BoundaryTypes boundaryTypesAtPos(const GlobalPosition &globalPos) const
    {
        BoundaryTypes bcTypes;

        bcTypes.setAllNeumann();

        return bcTypes;
    }

    NumEqVector source(const Element &element,
                       const FVElementGeometry& fvGeometry,
                       const ElementVolumeVariables& elemVolVars,
                       const SubControlVolume &scv) const
    {
        // declare source vector
        NumEqVector source;
        // extract priVars
        const auto& priVars = elemVolVars[scv].priVars();
        // For ODE system activities f are updated globally when u is.
        // Update both f and g for PDE formulation.
        Scalar f_D = 0.0;
        Scalar f_P = 0.0;
#if !ODE
        if (priVars[uAIdx] > ua_threshold_)
            f_D = priVars[uBIdx] / priVars[uAIdx] - 1;
        else
            f_D = priVars[uBIdx] / ua_threshold_ - 1;
        f_P = priVars[uBIdx] * priVars[uCIdx] - 1;
#else
        f_D = f_D_;
        f_P = f_P_;
#endif

        source[p1Idx] = 16.0/3 * omega_ * (
                -2*(priVars[p1Idx] -3*priVars[p1Idx]*priVars[p1Idx] +2*priVars[p1Idx]*priVars[p1Idx]*priVars[p1Idx])
                +  (priVars[p2Idx] -3*priVars[p2Idx]*priVars[p2Idx] +2*priVars[p2Idx]*priVars[p2Idx]*priVars[p2Idx])
                +  (priVars[p3Idx] -3*priVars[p3Idx]*priVars[p3Idx] +2*priVars[p3Idx]*priVars[p3Idx]*priVars[p3Idx])
                )
            -4 * xi_ * priVars[p1Idx] * priVars[p2Idx] * f_D * alpha_
            -4 * xi_ * priVars[p1Idx] * priVars[p3Idx] * f_P * alpha_;
        source[p2Idx] = 16.0/3 * omega_ * (
                   (priVars[p1Idx] -3*priVars[p1Idx]*priVars[p1Idx] +2*priVars[p1Idx]*priVars[p1Idx]*priVars[p1Idx])
                -2*(priVars[p2Idx] -3*priVars[p2Idx]*priVars[p2Idx] +2*priVars[p2Idx]*priVars[p2Idx]*priVars[p2Idx])
                +  (priVars[p3Idx] -3*priVars[p3Idx]*priVars[p3Idx] +2*priVars[p3Idx]*priVars[p3Idx]*priVars[p3Idx])
                )
            +4 * xi_ * priVars[p1Idx] * priVars[p2Idx] * f_D * alpha_;
        source[p3Idx] = 16.0/3 * omega_ * (
                   (priVars[p1Idx] -3*priVars[p1Idx]*priVars[p1Idx] +2*priVars[p1Idx]*priVars[p1Idx]*priVars[p1Idx])
                +  (priVars[p2Idx] -3*priVars[p2Idx]*priVars[p2Idx] +2*priVars[p2Idx]*priVars[p2Idx]*priVars[p2Idx])
                -2*(priVars[p3Idx] -3*priVars[p3Idx]*priVars[p3Idx] +2*priVars[p3Idx]*priVars[p3Idx]*priVars[p3Idx])
                )
            +4 * xi_ * priVars[p1Idx] *priVars[p3Idx] * f_P * alpha_;
#if !ODE
        source[uAIdx] = 0.0;
        source[uBIdx] = 0.0;
        source[uCIdx] = 0.0;
#endif

        return source;
    }

    PrimaryVariables initialAtPos(const GlobalPosition &globalPos) const
    {
        PrimaryVariables values;

        Scalar s = (globalPos[0] - 0.5)*(globalPos[0]-0.5)+(globalPos[1]-0.5)*(globalPos[1]-0.5) -
            0.2*0.2;
        Scalar s2 = (globalPos[0] - 0.5);
        values[p1Idx] = 1.0/(1.0 + std::exp(-10*s/xi_));                                         
        values[p2Idx] = (1.0-values[p1Idx])/(1.0 + std::exp(-3.8*s2/xi_));
        values[p3Idx] = 1.0 - values[p1Idx] - values[p2Idx];
        {
#if !ODE
            values[uAIdx] = 2.0;
            values[uBIdx] = 1.0;
            values[uCIdx] = 1.0;
#endif
        }
        return values;
    }

    void initTimeDerivative(std::shared_ptr<SolutionVector> xOld, TimeLoopPtr timeLoop)
    {
        xOld_ = xOld;
        timeLoop_ = timeLoop;
    }

    void initODE()
    {
        u_ = {2.0, 1.0, 1.0};
        f_D_ = -0.5;
        f_P_ = 0;
        A_ = Matrix(0);
    }

    void writeODE(std::ofstream &fout)
    {
        fout << timeLoop_->time() << '\t' << u_[0] << "\t" << u_[1] << "\t" << u_[2] << std::endl;;
    }

    void writeScalars(std::ofstream &fout)
    {
        fout << std::setprecision(10) << timeLoop_->time() << '\t'
            << volumeF_ << "\t" << volumeD_ << "\t" << volumeP_ << '\t'
            << sigmaF_ << '\t' << sigmaD_ << '\t' << sigmaP_ << '\t'
            << conservationB_ << '\t' << conservationC_ << std::endl;
    }

    void calculateGlobalScalarMeasures(const std::shared_ptr<SolutionVector> xPtr)
    {
        NumEqVector dummyFlux(0.0);
        calculateGlobalScalarMeasures(xPtr, dummyFlux);
    }

    void calculateGlobalScalarMeasures(const std::shared_ptr<SolutionVector> xPtr, NumEqVector
            boundaryFlux)
    {
        // Approximation of surface area
        newVolumeF_ = 0.0;
        newVolumeD_ = 0.0;
        newVolumeP_ = 0.0;
        sigmaF_ = 0.0;
        sigmaD_ = 0.0;
        sigmaP_ = 0.0;
        conservationB_ = 0.0;
        conservationC_ = 0.0;
#if !ODE
        accumOutFluxB_ += boundaryFlux[Indices::uBIdx];
        accumOutFluxC_ += boundaryFlux[Indices::uCIdx];
#endif
        for (auto dof : *xPtr)
        {
            Scalar sf =  dof[Indices::p1Idx] * (1-dof[Indices::p1Idx]);
            Scalar sd =  dof[Indices::p1Idx] * dof[Indices::p2Idx];
            Scalar sp =  dof[Indices::p1Idx] * dof[Indices::p3Idx];
            newVolumeF_ += dof[Indices::p1Idx] * hx_*hy_;
            newVolumeD_ += dof[Indices::p2Idx] * hx_*hy_;
            newVolumeP_ += dof[Indices::p3Idx] * hx_*hy_;
            sigmaF_ += 4.0*hx_*hy_/xi_*sf;
            sigmaD_ += 4.0*hx_*hy_/xi_*sd;
            sigmaP_ += 4.0*hx_*hy_/xi_*sp;
#if !ODE
            conservationB_ += hx_*hy_ * dof[Indices::p1Idx] * dof[Indices::uBIdx];
            conservationC_ += hx_*hy_ * dof[Indices::p1Idx] * dof[Indices::uCIdx];
#endif
        }
    }

    // for PDE version where old volumes are not required
    void updateGlobalValues()
    {
        volumeF_ = newVolumeF_;
        volumeD_ = newVolumeD_;
        volumeP_ = newVolumeP_;
#if ODE
        // only for ODE, called after advancing ODE
        conservationB_ = u_[1] * newVolumeF_;
        conservationC_ = u_[2] * newVolumeF_;
#endif
        // shared update of conservation from volumes
        conservationB_ += rhoD_ * newVolumeD_ + rhoP_ * newVolumeP_ + accumOutFluxB_;
        conservationC_ += rhoP_ * newVolumeP_ + accumOutFluxC_;
    }

    // advance coupled ODE, calculating u_. Requires old volumes, to be overwritten after
    void advanceODE(const std::shared_ptr<SolutionVector> xPtr)
    {
        //updateGlobalScalarMeasures(xPtr);
        Vector uk{}, r{};
        // explicit Euler for initial guess (reaction based) or solution (conservation based)
        if (u_[0] > ua_threshold_)
            f_D_ = u_[1] / u_[0] - 1;
        else
            f_D_ = u_[1] / ua_threshold_ - 1;
        f_P_ = u_[1] * u_[2] - 1;
        uk[0] = 1.0/newVolumeF_ * (u_[0]*volumeF_
            - ((-1)*rhoD_ * (newVolumeD_ - volumeD_))
            );
        uk[1] = 1.0/newVolumeF_ * (u_[1]*volumeF_
            - 1.0 * (rhoD_ * (newVolumeD_ - volumeD_) + rhoP_ * (newVolumeP_ - volumeP_))
            );
        uk[2] = 1.0/newVolumeF_ * (u_[2]*volumeF_
            - (rhoP_ * (newVolumeP_ - volumeP_))
            );
# if ODE_REACTION
        Scalar dt = timeLoop_->timeStepSize();
        volumeF_ = newVolumeF_;
        uk[0] = 1.0/newVolumeF_ * (u_[0]*volumeF_
            - dt * (sigmaD_*f_D_*(-rhoD_ -u_[0])
                  + sigmaP_*f_P_*(       -u_[0]))
            );
        uk[1] = 1.0/newVolumeF_ * (u_[1]*volumeF_
            - dt * (sigmaD_*f_D_*( rhoD_ -u_[1])
                  + sigmaP_*f_P_*( rhoP_ -u_[1]))
            );
        uk[2] = 1.0/newVolumeF_ * (u_[2]*volumeF_
            - dt * (sigmaD_*f_D_*(       -u_[2])
                  + sigmaP_*f_P_*( rhoP_ -u_[2]))
            );
        Vector d;
        Scalar w = 1.0;
        for (int i=0; i<8; i++)
        {
            r = residual(uk);
            d = step(uk, r);
            d *= w;
            uk -= d;
            Scalar relativeShift = d.infinity_norm();
            if (relativeShift < 1e-8)
            {
                std::cout << "uk = (" << uk[0] << ", " << uk[1] << ", "  << uk[2] << "), r = " << r.two_norm() << '\n';
                break;
            }
        }
# endif
        u_[0] = uk[0];
        u_[1] = uk[1];
        u_[2] = uk[2];
    }

    Vector residual(const Vector& uk)
    {
        Vector r{};
        Scalar dt = timeLoop_->timeStepSize();
        if (uk[0] > ua_threshold_)
            f_D_ = uk[1] / uk[0] - 1;
        else
            f_D_ = uk[1] / ua_threshold_ - 1;
        f_P_ = uk[1] * uk[2] - 1;
        Scalar dd = dt * sigmaD_ * f_D_;
        Scalar dp = dt * sigmaP_ * f_P_;
        r[0] = uk[0] + 1.0/newVolumeF_*( - u_[0]*volumeF_
            + ( dd*(-rhoD_ -uk[0]) + dp*(      -uk[0]))
            );
        r[1] = uk[1] + 1.0/newVolumeF_*( - u_[1]*volumeF_
            + ( dd*( rhoD_ -uk[1]) + dp*(rhoP_ -uk[1]))
            );
        r[2] = uk[2] + 1.0/newVolumeF_*( - u_[2]*volumeF_
            + ( dd*(       -uk[2]) + dp*(rhoP_ -uk[2]))
            );
        return r;
    }

    Vector step(const Vector& uk, const Vector& r)
    {
        Vector d{};
        Scalar dt = timeLoop_->timeStepSize();
        // approximate Fluid volume to linearise in u
        const static Scalar volumeF = volumeF_;
        //                         surface    df_{M,i}/du_j          trapped, dissolved
        A_[0][0] = dt / volumeF * (sigmaD_ * (-1*uk[1]/(uk[0]*uk[0])*(-rhoD_ -uk[0])
                                              -  uk[1]/uk[0]        *         uk[0])
                                  +sigmaP_ * ( 0
                                              -  uk[1]*uk[2]        *         uk[0])
                                  ) + 1;
        A_[0][1] = dt / volumeF * (sigmaD_ * (   1/uk[0]            *(-rhoD_ -uk[0]))
                                  +sigmaP_ * (   uk[2]              *(0      -uk[0])));
        A_[0][2] = dt / volumeF * (sigmaD_ * ( 0                    *(-rhoD_ -uk[0]))
                                  +sigmaP_ * (   uk[1]              *(0      -uk[0])));

        A_[1][0] = dt / volumeF * (sigmaD_ * (-1*uk[1]/(uk[0]*uk[0])*(rhoD_  -uk[1]))
                                  +sigmaP_ * ( 0                    *(rhoD_  -uk[1])));
        A_[1][1] = dt / volumeF * (sigmaD_ * (   1/uk[0]            *(rhoD_  -uk[1])
                                              -  uk[1]/uk[0]        *         uk[1] )
                                  +sigmaP_ * (   uk[2]              *(rhoP_  -uk[1])
                                              -  uk[1]*uk[2]        *         uk[1] )
                                  ) + 1;
        A_[1][2] = dt / volumeF * (sigmaD_ * ( 0 )
                                  +sigmaP_ * (   uk[1]              *(rhoP_  -uk[1])));

        A_[2][0] = dt / volumeF * (sigmaD_ * (-1*uk[1]/(uk[0]*uk[0])*(0      -uk[2]))
                                  +sigmaP_ * ( 0                    *(rhoP_  -uk[2])));
        A_[2][1] = dt / volumeF * (sigmaD_ * (   1/uk[0]            *(0      -uk[2]))
                                  +sigmaP_ * (   uk[2]              *(rhoP_  -uk[2])));
        A_[2][2] = dt / volumeF * (sigmaD_ * ( 0                    *(0      -uk[2])
                                              -  uk[1]/uk[0]        *         uk[2] )
                                  +sigmaP_ * (   uk[1]              *(rhoP_  -uk[2])
                                              -  uk[1]*uk[2]        *         uk[2] )
                                  )+ 1;
        A_.solve(d, r);
        return d;
    }

    Scalar getAlpha() const
    {
        return alpha_;
    }

    Scalar getOmega() const
    {
        return omega_;
    }

private:
    Scalar xi_;
    Scalar omega_;
    Scalar alpha_;
    Scalar rhoD_;
    Scalar rhoP_;
    std::shared_ptr<SolutionVector> xOld_;
    TimeLoopPtr timeLoop_;
    static constexpr Scalar ua_threshold_ = 1e-9;
    static constexpr Scalar p1_threshold_ = 1e-9;
    Vector u_;
    Matrix A_;
    Scalar f_D_, f_P_;
    Scalar volumeF_, volumeD_, volumeP_;
    Scalar newVolumeF_, newVolumeD_, newVolumeP_;
    Scalar sigmaF_, sigmaD_, sigmaP_;
    Scalar hx_, hy_;
    Scalar conservationB_, conservationC_;
    Scalar accumOutFluxB_, accumOutFluxC_;
};

} //end namespace Dumux

#endif // DUMUX_PLAINALLENCAHN_PROBLEM_HH
